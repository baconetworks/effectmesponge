package baco.config;

import org.spongepowered.api.effect.particle.ParticleEffect;
import org.spongepowered.api.effect.particle.ParticleTypes;

import java.util.ArrayList;

public class effects {
    public static ParticleEffect fire = ParticleEffect.builder().type(ParticleTypes.FLAME).quantity(4).build();
    public static ParticleEffect ender = ParticleEffect.builder().type(ParticleTypes.DRAGON_BREATH).quantity(4).build();
    public static ParticleEffect smoke = ParticleEffect.builder().type(ParticleTypes.LARGE_SMOKE).quantity(8).build();
    public static ParticleEffect heart = ParticleEffect.builder().type(ParticleTypes.HEART).quantity(2).build();
    public static ParticleEffect Snow = ParticleEffect.builder().type(ParticleTypes.SNOW_SHOVEL).quantity(4).build();
    public static ParticleEffect Note = ParticleEffect.builder().type(ParticleTypes.NOTE).quantity(2).build();
    public static ParticleEffect Explosion = ParticleEffect.builder().type(ParticleTypes.LARGE_EXPLOSION).quantity(1).build();
    public static ParticleEffect Magic = ParticleEffect.builder().type(ParticleTypes.WITCH_SPELL).quantity(4).build();

    public static ArrayList<String> listPlayersFire = new ArrayList();
    public static ArrayList<String> listPlayersEnder = new ArrayList();
    public static ArrayList<String> listPlayersSmoke = new ArrayList();
    public static ArrayList<String> listPlayersHeart = new ArrayList();
    public static ArrayList<String> listPlayersSnow = new ArrayList();
    public static ArrayList<String> listPlayersNote = new ArrayList();
    public static ArrayList<String> listPlayersExplosion = new ArrayList();
    public static ArrayList<String> listPlayersMagic = new ArrayList();
}
