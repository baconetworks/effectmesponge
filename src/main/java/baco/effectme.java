package baco;

import baco.commands.*;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import static baco.config.effects.*;


@Plugin(id = "effectme", name = "EffectMe", version =" 1.0")
public class effectme{

    private void makeCommands() {
        CommandSpec fire = CommandSpec.builder().description(Text.of("Fire Effect")).permission("baco.effectme.fire").executor(new effectFire()).build();
        CommandSpec ender = CommandSpec.builder().description(Text.of("Ender Effect")).permission("baco.effectme.ender").executor(new effectEnder()).build();
        CommandSpec smoke = CommandSpec.builder().description(Text.of("Smoke Effect")).permission("baco.effectme.smoke").executor(new effectSmoke()).build();
        CommandSpec heart = CommandSpec.builder().description(Text.of("Heart Effect")).permission("baco.effectme.heart").executor(new effectHeart()).build();
        CommandSpec snow = CommandSpec.builder().description(Text.of("Snow Effect")).permission("baco.effectme.snow").executor(new effectSnow()).build();
        CommandSpec note = CommandSpec.builder().description(Text.of("Note Effect")).permission("baco.effectme.note").executor(new effectNote()).build();
        CommandSpec explosion = CommandSpec.builder().description(Text.of("Explosion Effect")).permission("baco.effectme.explosion").executor(new effectExplosion()).build();
        CommandSpec magic = CommandSpec.builder().description(Text.of("Magic Effect")).permission("baco.effectme.magic").executor(new effectMagic()).build();
        CommandSpec disable = CommandSpec.builder().description(Text.of("Disable Effects")).permission("baco.effectme.disable").executor(new effectDisable()).build();
        CommandSpec effectme = CommandSpec.builder().description(Text.of("Master Command")).permission("baco.effectme").executor(new effectCMD()).child(fire, "fire").child(ender, "ender").child(smoke, "smoke").child(snow, "snow").child(heart, "heart").child(note, "note").child(explosion, "explosion").child(magic, "magic").child(disable, "disable").build();
        Sponge.getCommandManager().register(this, effectme, "effectme");
    }

    @Listener
    public void onWalkFire (MoveEntityEvent e, @First Player player){
        if (listPlayersFire.contains(player.getName())){
            player.getWorld().spawnParticles(fire, player.getLocation().getPosition());
        }
    }

    @Listener
    public void onWalkEnder (MoveEntityEvent e, @First Player player){
        if (listPlayersEnder.contains(player.getName())){
            player.getWorld().spawnParticles(ender, player.getLocation().getPosition());
        }
    }

    @Listener
    public void onWalkSmoke (MoveEntityEvent e, @First Player player){
        if (listPlayersSmoke.contains(player.getName())){
            player.getWorld().spawnParticles(smoke, player.getLocation().getPosition());
        }
    }

    @Listener
    public void onWalkHeart (MoveEntityEvent e, @First Player player){
        if (listPlayersHeart.contains(player.getName())){
            player.getWorld().spawnParticles(heart, player.getLocation().getPosition());
        }
    }

    @Listener
    public void onWalkSnow (MoveEntityEvent e, @First Player player){
        if (listPlayersSnow.contains(player.getName())){
            player.getWorld().spawnParticles(Snow, player.getLocation().getPosition());
        }
    }

    @Listener
    public void onWalkNote (MoveEntityEvent e, @First Player player){
        if (listPlayersNote.contains(player.getName())){
            player.getWorld().spawnParticles(Note, player.getLocation().getPosition());
        }
    }

    @Listener
    public void onWalkExplosion (MoveEntityEvent e, @First Player player){
        if (listPlayersExplosion.contains(player.getName())){
            player.getWorld().spawnParticles(Explosion, player.getLocation().getPosition());
        }
    }

    @Listener
    public void onWalkMagic (MoveEntityEvent e, @First Player player){
        if (listPlayersMagic.contains(player.getName())){
            player.getWorld().spawnParticles(Magic, player.getLocation().getPosition());
        }
    }

    @Listener
    public void onInit(GameStartedServerEvent e){
        makeCommands();
    }

}
