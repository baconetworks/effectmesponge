package baco.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import static baco.config.effects.*;

public class effectMagic implements CommandExecutor {
    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if (src instanceof Player) {
            Player player = (Player) src;
            listPlayersEnder.remove(player.getName());
            listPlayersSmoke.remove(player.getName());
            listPlayersFire.remove(player.getName());
            listPlayersHeart.remove(player.getName());
            listPlayersSnow.remove(player.getName());
            listPlayersNote.remove(player.getName());
            listPlayersExplosion.remove(player.getName());
            listPlayersMagic.add(player.getName());
        }
        return CommandResult.success();
    }
}