package baco.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.pagination.PaginationList;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextStyles;

public class effectCMD implements CommandExecutor {
    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if (src instanceof Player) {
            Player player = (Player) src;
            Text Toggle = Text.builder("Disable Effect").onClick(TextActions.runCommand("/effectme disable")).color(TextColors.RED).onHover(TextActions.showText(Text.of("Disable Effects"))).style(TextStyles.BOLD).build();
            Text Fire = Text.builder("Fire Effect").onClick(TextActions.runCommand("/effectme fire")).color(TextColors.GREEN).onHover(TextActions.showText(Text.of("Enable Fire Effect"))).style(TextStyles.BOLD).build();
            Text Ender = Text.builder("Ender Effect").onClick(TextActions.runCommand("/effectme ender")).color(TextColors.GREEN).onHover(TextActions.showText(Text.of("Enable Ender Effect"))).style(TextStyles.BOLD).build();
            Text Smoke = Text.builder("Smoke Effect").onClick(TextActions.runCommand("/effectme smoke")).color(TextColors.GREEN).onHover(TextActions.showText(Text.of("Enable Smoke Effect"))).style(TextStyles.BOLD).build();
            Text Heart = Text.builder("Heart Effect").onClick(TextActions.runCommand("/effectme heart")).color(TextColors.GREEN).onHover(TextActions.showText(Text.of("Enable Heart Effect"))).style(TextStyles.BOLD).build();
            Text Snow = Text.builder("Snow Effect").onClick(TextActions.runCommand("/effectme snow")).color(TextColors.GREEN).onHover(TextActions.showText(Text.of("Enable Snow Effect"))).style(TextStyles.BOLD).build();
            Text Note = Text.builder("Note Effect").onClick(TextActions.runCommand("/effectme note")).color(TextColors.GREEN).onHover(TextActions.showText(Text.of("Enable Note Effect"))).style(TextStyles.BOLD).build();
            Text Explosion = Text.builder("Explosion Effect").onClick(TextActions.runCommand("/effectme explosion")).color(TextColors.GREEN).onHover(TextActions.showText(Text.of("Enable Explosion Effect"))).style(TextStyles.BOLD).build();
            Text Magic = Text.builder("Magic Effect").onClick(TextActions.runCommand("/effectme magic")).color(TextColors.GREEN).onHover(TextActions.showText(Text.of("Enable Magic Effect"))).style(TextStyles.BOLD).build();
            PaginationList.builder()
                    .title(Text.of(TextStyles.BOLD, TextColors.GREEN, "EffectMe"))
                    .contents(Toggle, Fire, Ender, Smoke, Heart, Snow, Note, Explosion, Magic)
                    .padding(Text.of("="))
                    .sendTo(player);
        }
        return CommandResult.success();
    }
}