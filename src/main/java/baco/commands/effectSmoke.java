package baco.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import static baco.config.effects.*;

public class effectSmoke implements CommandExecutor {
    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if (src instanceof Player) {
            Player player = (Player) src;
            listPlayersEnder.remove(player.getName());
            listPlayersSmoke.add(player.getName());
            listPlayersFire.remove(player.getName());
            listPlayersHeart.remove(player.getName());
            listPlayersSnow.remove(player.getName());
            listPlayersNote.remove(player.getName());
            listPlayersExplosion.remove(player.getName());
            listPlayersMagic.remove(player.getName());
        }
        return CommandResult.success();
    }
}